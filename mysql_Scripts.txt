
-- -----------------------------------------------------
-- Schema bds-db-design
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bds-db-design
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bds-db-design` DEFAULT CHARACTER SET utf8mb4 ;
USE `bds-db-design` ;

-- -----------------------------------------------------
-- Table `bds-db-design`.`Employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Employee` (
  `employee_id` INT(8) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`employee_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Customer` (
  `customer_id` INT(8) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`customer_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Product` (
  `product_id` INT(8) NOT NULL AUTO_INCREMENT,
  `product_name` VARCHAR(20) NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`product_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Order` (
  `order_id` INT(8) NOT NULL AUTO_INCREMENT,
  `order_status` VARCHAR(20) NOT NULL,
  `date` DATE NOT NULL,
  `customer_id` INT(8) NOT NULL,
  PRIMARY KEY (`order_id`, `customer_id`),
  INDEX `fk_Order_Customer_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Order_Customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bds-db-design`.`Customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Role` (
  `role_id` INT(8) NOT NULL AUTO_INCREMENT,
  `role_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Customer_has_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Customer_has_order` (
  `order_id` INT(8) NOT NULL,
  `product_id` INT(8) NOT NULL,
  PRIMARY KEY (`order_id`, `product_id`),
  INDEX `fk_Order_has_Product_Product1_idx` (`product_id` ASC) VISIBLE,
  INDEX `fk_Order_has_Product_Order1_idx` (`order_id` ASC) VISIBLE,
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `bds-db-design`.`Order` (`order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `bds-db-design`.`Product` (`product_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Employee_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Employee_role` (
  `employee_id` INT(8) NOT NULL,
  `role_id` INT(8) NOT NULL,
  PRIMARY KEY (`employee_id`, `role_id`),
  INDEX `fk_Employee_has_Role_Role1_idx` (`role_id` ASC) VISIBLE,
  INDEX `fk_Employee_has_Role_Employee1_idx` (`employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_Employee_has_Role_Employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `bds-db-design`.`Employee` (`employee_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_has_Role_Role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `bds-db-design`.`Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Customer_contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Customer_contact` (
  `contact_id` INT(8) NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `customer_id` INT(8) NOT NULL,
  PRIMARY KEY (`contact_id`, `customer_id`),
  INDEX `fk_Customer_contact_Customer1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Customer_contact_Customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bds-db-design`.`Customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Address` (
  `address_id` INT(8) NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `house_number` VARCHAR(45) NOT NULL,
  `zip_code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`address_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Customer_has_Address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Customer_has_Address` (
  `customer_id` INT(8) NOT NULL,
  `address_id` INT(8) NOT NULL,
  PRIMARY KEY (`customer_id`, `address_id`),
  INDEX `fk_Customer_has_Address_Address1_idx` (`address_id` ASC) VISIBLE,
  INDEX `fk_Customer_has_Address_Customer1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Customer_has_Address_Customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bds-db-design`.`Customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Customer_has_Address_Address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `bds-db-design`.`Address` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Employee_training`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Employee_training` (
  `training_id` INT(8) NOT NULL AUTO_INCREMENT,
  `theme` VARCHAR(45) NOT NULL,
  `date` DATE NOT NULL,
  `start` TIME NOT NULL,
  `end` TIME NOT NULL,
  PRIMARY KEY (`training_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Employee_has_training`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Employee_has_training` (
  `employee_id` INT(8) NOT NULL,
  `training_id` INT(8) NOT NULL,
  PRIMARY KEY (`employee_id`, `training_id`),
  INDEX `fk_Employee_has_Employee_training_Employee_training1_idx` (`training_id` ASC) VISIBLE,
  INDEX `fk_Employee_has_Employee_training_Employee1_idx` (`employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_Employee_has_Employee_training_Employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `bds-db-design`.`Employee` (`employee_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employee_has_Employee_training_Employee_training1`
    FOREIGN KEY (`training_id`)
    REFERENCES `bds-db-design`.`Employee_training` (`training_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Manager`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Manager` (
  `manager_id` INT(8) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `manager_tier` VARCHAR(45) NOT NULL,
  `Role_role_id` INT(8) NOT NULL,
  PRIMARY KEY (`manager_id`, `Role_role_id`),
  INDEX `fk_Manager_Role1_idx` (`Role_role_id` ASC) VISIBLE,
  CONSTRAINT `fk_Manager_Role1`
    FOREIGN KEY (`Role_role_id`)
    REFERENCES `bds-db-design`.`Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bds-db-design`.`Manager_employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bds-db-design`.`Manager_employees` (
  `manager_id` INT(8) NOT NULL,
  `employee_id` INT(8) NOT NULL,
  PRIMARY KEY (`manager_id`, `employee_id`),
  INDEX `fk_Manager_has_Employee_Employee1_idx` (`employee_id` ASC) VISIBLE,
  INDEX `fk_Manager_has_Employee_Manager1_idx` (`manager_id` ASC) VISIBLE,
  CONSTRAINT `fk_Manager_has_Employee_Manager1`
    FOREIGN KEY (`manager_id`)
    REFERENCES `bds-db-design`.`Manager` (`manager_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Manager_has_Employee_Employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `bds-db-design`.`Employee` (`employee_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
